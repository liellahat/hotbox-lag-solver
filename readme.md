# Description

This program solves lags in hotbox modems by renewing the WAN lease when a ping threshold is crossed.
The program won't reduce ping within the LAN, it's only effective in the WAN.


# Configuration

Some parameters can be configured in the config.json file:

hotbox_ip - IP address of the modem's configuration interface (probably the default gateway).
ping_host - IP address for ping testing.
ping_number - how many times to ping (each ping takes 1 second) before comparing the average result to the threshold.
ping_threshold - the amount of ping that is considered a lag, that needs to be solved.
username (optional) - lag solver is more reliable if credentials are configured.
password (optional) - ^^


# Build

Run this command:

setup.bat