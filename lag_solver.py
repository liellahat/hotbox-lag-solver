import requests


class BadResponseCode(Exception):
    pass

class CouldNotGetCsrfToken(Exception):
    pass

class BadResponseContent(Exception):
    pass

class LogonFailed(Exception):
    pass


class LagSolver:

    def __init__(self, hotbox_ip: str):
        self._hotbox_ip = hotbox_ip
        self._default_headers = {
            'Host': self._hotbox_ip,
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'DNT': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Referer': 'http://{}/RgSwInfo.asp'.format(self._hotbox_ip),
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'he-IL,he;q=0.9,en-US;q=0.8,en;q=0.7',
            'Cookie': 'login='
        }

    def _get_csrf_key(self, path, csrf_name):
        """
        receives url path and name of the csrf tag. returns the csrf key.
        :param path:
        :param csrf_name:
        :return:
        """

        r = requests.get(f'http://{self._hotbox_ip}/{path}', headers=self._default_headers)
        html = r.content.decode()

        return self._parse_csrf_key(html, csrf_name)

    def _parse_csrf_key(self, html, csrf_name):
        """
        receives raw html and the name of the csrf tag. returns the csrf key.
        :param html:
        :param csrf_name:
        :return:
        """
        csrf_tag = f'<input type="hidden" name="{csrf_name}" value='
        csrf_tag_index = html.find(csrf_tag)

        if csrf_tag_index == -1:
            raise CouldNotGetCsrfToken

        csrf_key = ''

        for c in html[csrf_tag_index + len(csrf_tag):]:
            if c == ' ':
                break
            csrf_key += c

        return csrf_key

    # region logon

    def logon(self, username: str, password: str):
        """
        receives credentials and performs logon. returns true if got 200.
        :param username:
        :param password:
        :return:
        """
        # get csrf token from index for logon
        csrf_key = self._get_csrf_key('', 'LoginCsrfToken')

        # logon
        r = requests.post(f'http://{self._hotbox_ip}/goform/login',
                          data=self._get_logon_data(username, password, csrf_key),
                          headers=self._get_logon_request_headers())

        if not r.ok:
            raise BadResponseCode(r.status_code)

        if not 'rgswinfo.asp' in r.url.lower():
            raise LogonFailed(r.content)

    def _get_logon_request_headers(self):
        """
        returns headers for logon post request as dict.
        :return:
        """
        headers = dict(self._default_headers)
        headers['Content-Type'] = 'application/x-www-form-urlencoded'
        headers['Referer'] = f'http://{self._hotbox_ip}/'
        headers['Origin'] = f'http://{self._hotbox_ip}/'
        return headers

    def _get_logon_data(self, username, password, csrf_key):
        """
        returns data for post request as string.
        :param username:
        :param password:
        :param csrf_key:
        :return:
        """
        return f'loginUsername={username}&loginPassword={password}&LoginCsrfToken={csrf_key}'

    # endregion logon

    # region solve-lag

    def solve_lag(self):
        """
        solves lag by submitting a renew wan lease request.
        :return:
        """

        csrf_key = self._get_csrf_key('RgSetup.asp', 'SetupCsrfToken')

        r =  requests.post('http://{}/goform/RgSetup'.format(self._hotbox_ip),
                           data=self._get_renew_wan_lease_form_data(csrf_key),
                           headers=self._get_renew_wan_lease_request_headers())

        if not r.ok:
            raise BadResponseCode

        if not len(r.content) > 3000:
            raise BadResponseContent(r.content)

    def _get_renew_wan_lease_form_data(self, csrf_key):
        """
        returns data for renew wan lease post request.
        :param csrf_key:
        :return:
        """

        ip_parts = self._hotbox_ip.split('.')

        return f'LocalIpAddressIP0={ip_parts[0]}&' \
               f'LocalIpAddressIP1={ip_parts[1]}&' \
               f'LocalIpAddressIP2={ip_parts[2]}&' \
               f'LocalIpAddressIP3={ip_parts[3]}&' \
               f'WanLeaseAction=1&' \
               f'WanConnectionType=0&' \
               f'MtuSize=0&' \
               f'RestoreFactoryNo1=0x00&' \
               f'ApplyRgSetupAction=0&' \
               f'ApplyRebootAction=0&' \
               f'SetupCsrfToken={csrf_key}'\

    def _get_renew_wan_lease_request_headers(self):
        """
        returns request headers for renew wan lease request as dict.
        :return:
        """
        headers = dict(self._default_headers)
        headers['Content-Type'] = 'application/x-www-form-urlencoded'
        return headers

    # endregion solve-lag