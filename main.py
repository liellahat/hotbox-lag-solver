from lag_solver import LagSolver
import subprocess
import json
import pyfiglet
import time
import keyboard
import logging
import _thread


interrupt = False

def print_description():
    print(pyfiglet.figlet_format('HotBox Lag Solver'))
    print()
    print('Version: 1.0')
    print()
    print('Hot support is hanging up on their clients, so sometimes you gotta do things yourself.')
    print()
    print('Author: Liel Lahat')
    print()
    print('NOTE: The Lag Solver works more reliably if the user provides credentials (read the readme file).')
    print()
    print('-------------------------------------------')

    print()

def get_config():
    with open('config.json', 'r') as config:
        return json.load(config)

def login(solver: LagSolver, username: str, password: str):
    try:
        solver.logon(username, password)
        print('Successful logon.')
        print()

    except Exception as e:
        print('Logon failed. Exiting.')
        print()
        logging.error(repr(e))
        exit()

def listen_for_interrupt():
    while True:
        if keyboard.is_pressed('ctrl + shift + q'):
            global interrupt
            interrupt = True

def get_avg_ping(ping_output):
    output_as_str = ping_output.stdout.read().decode()
    avg_unparsed = output_as_str[output_as_str.rfind('=') + 2:]
    avg_str = avg_unparsed[:avg_unparsed.find('m')]

    return int(avg_str)

def wait_for_lag(solver: LagSolver,
                 ping_host: str,
                 ping_number: str,
                 ping_threshold: int):

    print('Waiting for lag... (press ctrl + shift + q to exit)')
    print()

    _thread.start_new(listen_for_interrupt, ())

    lags_solved = 0
    fails = 0

    while not interrupt:
        ping_command = ['ping', '-n', ping_number, ping_host]

        output = subprocess.Popen(ping_command, stdout=subprocess.PIPE)

        avg_ping = get_avg_ping(output)

        if avg_ping > ping_threshold:
            print(f'\rLags solved: {lags_solved}\tFails: {fails}\tAverage ping: {avg_ping} - solving lag...'.ljust(80), end='')

            try:
                solver.solve_lag()
                lags_solved += 1

            except Exception as e:
                fails += 1
                logging.error(repr(e))

            time.sleep(3)

        else:
            print(f'\rLags solved: {lags_solved}\tFails: {fails}\tAverage ping: {avg_ping}'.ljust(80), end='')

def main():
    try:
        logging.basicConfig(filename='error.log')
        print_description()

        config = get_config()

        hotbox_ip = config['hotbox_ip']
        ping_number = config['ping_number']
        ping_host = config['ping_host']
        ping_threshold = config['ping_threshold']

        username = config.get('username')
        password = config.get('password')

        solver = LagSolver(hotbox_ip)

        if username is not None and password is not None:
            login(solver, username, password)

        wait_for_lag(solver, ping_host, ping_number, ping_threshold)

    except Exception as e:
        logging.error(repr(e))
        print('Press enter to continue.')
        input()



if __name__ == '__main__':
    main()
